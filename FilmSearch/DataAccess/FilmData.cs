﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using FilmSearch.Models;

namespace FilmSearch.DataAccess
{
	public class FilmData
	{
		public FilmData() { }


		public IList<Film> GetFilms()
		{
			return GetFilms(string.Empty);
		}

		public IList<Film> GetFilms(string titleSearch)
		{
			List<Film> films = new List<Film>();

			using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["TitlesEntities"].ConnectionString))
			{
				conn.Open();


				StringBuilder sql = new StringBuilder();
				sql.AppendLine("SELECT TitleId, TitleName, TitleNameType, TitleNameLanguage, TitleNameSortable, ReleaseYear ");
				sql.AppendLine("FROM (");
				sql.AppendLine("		SELECT TitleId, TitleName, TitleNameType, TitleNameLanguage, ReleaseYear, TitleNameSortable, TitleTypeSortable, TitleLanguageSortable, ROW_NUMBER() OVER (PARTITION BY TitleId ORDER BY TitleLanguageSortable, TitleTypeSortable, TitleNameSortable) RowRank ");
				sql.AppendLine("		FROM (");
				sql.AppendLine("				SELECT TitleId, TitleName, 'Primary' TitleNameType, 'ENGLISH' TitleNameLanguage, ReleaseYear, TitleNameSortable, 1 TitleTypeSortable, 1 TitleLanguageSortable FROM Title UNION ");
				sql.AppendLine("				SELECT TitleId, TitleName, TitleNameType, TitleNameLanguage, (SELECT TOP 1 ReleaseYear FROM Title WHERE TitleID = OtherName.TitleID) ReleaseYear, TitleNameSortable, CASE TitleNameType WHEN 'Primary' THEN 1 ELSE 2 END TitleTypeSortable, CASE TitleNameLanguage WHEN 'ENGLISH' THEN 1 ELSE 2 END TitleLanguageSortable FROM OtherName ");
				sql.AppendLine("			) Films ");

				if (!string.IsNullOrWhiteSpace(titleSearch))
				{
					sql.AppendLine(string.Format("		WHERE TitleName LIKE '{0}%' OR TitleNameSortable LIKE '{1}%' ", titleSearch, titleSearch));
				}

				sql.AppendLine("	) TitleSearch");
				sql.AppendLine("WHERE RowRank = 1");
				sql.AppendLine("ORDER BY TitleNameSortable");


				SqlCommand comm = new SqlCommand(sql.ToString(), conn);
				SqlDataReader dr = comm.ExecuteReader();
				if (dr.HasRows)
				{
					int _titleID;
					Film film;
					while (dr.Read())
					{
						film = new Film();
						if (!Int32.TryParse(dr["TitleID"].ToString(), out _titleID))
							continue;

						film.TitleID = _titleID;
						film.Title = new Title();
						film.Title.Name = dr["TitleName"].ToString();
						film.Title.Type = dr["TitleNameType"].ToString();
						film.Title.Language = dr["TitleNameLanguage"].ToString();
						film.Title.Sortable = dr["TitleNameSortable"].ToString();
						film.ReleaseYear = dr["ReleaseYear"].ToString();

						films.Add(film);
					}
				}

				dr.Close();
				dr.Dispose();

				comm.Dispose();
			}

			return films;
		}

		public IList<Film> GetFilmsDetail(List<int> titleID)
		{
			List<Film> films = new List<Film>();

			using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["TitlesEntities"].ConnectionString))
			{
				conn.Open();

				string sql = string.Format("SELECT TitleID, TitleName, 'Primary' TitleNameType, 'ENGLISH' TitleNameLanguage, TitleNameSortable, ReleaseYear FROM Title WHERE TitleID IN ({0}) ORDER BY TitleNameSortable", titleID.Select(i => i.ToString()).Aggregate((s1, s2) => s1 + ", " + s2));
				SqlCommand comm = new SqlCommand(sql, conn);
				SqlDataReader dr = comm.ExecuteReader();
				if (dr.HasRows)
				{
					int _titleID;
					Film film;
					while (dr.Read())
					{
						film = new Film();
						if (!Int32.TryParse(dr["TitleID"].ToString(), out _titleID))
							continue;

						film.TitleID = _titleID;
						film.Title = new Title();
						film.Title.Name = dr["TitleName"].ToString();
						film.Title.Type = dr["TitleNameType"].ToString();
						film.Title.Language = dr["TitleNameLanguage"].ToString();
						film.Title.Sortable = dr["TitleNameSortable"].ToString();
						film.ReleaseYear = dr["ReleaseYear"].ToString();
						film.AltTitles = this.GetFilmTitles(_titleID).ToList<Title>();
						film.Synopses = this.GetFilmSynopses(_titleID).ToList<Synopsis>();
						film.Genres = this.GetFilmGenres(_titleID).ToList<Genre>();
						film.Awards = this.GetFilmAwards(_titleID).ToList<Award>();
						film.Participants = this.GetFilmParticipants(_titleID).ToList<Participant>();

						films.Add(film);
					}
				}

				dr.Close();
				dr.Dispose();

				comm.Dispose();
			}

			return films;
		}


		private IList<Title> GetFilmTitles(int titleID)
		{
			List<Title> titles = new List<Title>();

			using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["TitlesEntities"].ConnectionString))
			{
				conn.Open();

				string sql = string.Format("SELECT TitleName, TitleNameType, TitleNameLanguage, TitleNameSortable FROM OtherName WHERE TitleID = '{0}' AND NOT EXISTS (SELECT 1 FROM Title WHERE OtherName.TitleName = Title.TitleName AND OtherName.TitleID = Title.TitleID) ORDER BY CASE TitleNameType WHEN 'Primary' THEN 1 ELSE 2 END, CASE TitleNameLanguage WHEN 'ENGLISH' THEN 1 ELSE 2 END, TitleNameSortable", titleID.ToString());
				SqlCommand comm = new SqlCommand(sql, conn);
				SqlDataReader dr = comm.ExecuteReader();
				if (dr.HasRows)
				{
					Title title;
					while (dr.Read())
					{
						title = new Title();
						title.Name = dr["TitleName"].ToString();
						title.Type = dr["TitleNameType"].ToString();
						title.Language = dr["TitleNameLanguage"].ToString();
						title.Sortable = dr["TitleNameSortable"].ToString();

						titles.Add(title);
					}
				}

				dr.Close();
				dr.Dispose();

				comm.Dispose();
			}

			return titles;
		}


		private IList<Synopsis> GetFilmSynopses(int titleID)
		{
			List<Synopsis> synopses = new List<Synopsis>();

			using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["TitlesEntities"].ConnectionString))
			{
				conn.Open();

				string sql = string.Format("SELECT Type, Language, Description FROM StoryLine WHERE TitleID = '{0}' ORDER BY Language, Type", titleID.ToString());
				SqlCommand comm = new SqlCommand(sql, conn);
				SqlDataReader dr = comm.ExecuteReader();
				if (dr.HasRows)
				{
					Synopsis synopsis;
					while (dr.Read())
					{
						synopsis = new Synopsis();
						synopsis.Type = dr["Type"].ToString();
						synopsis.Language = dr["Language"].ToString();
						synopsis.Description = dr["Description"].ToString();

						synopses.Add(synopsis);
					}
				}

				dr.Close();
				dr.Dispose();

				comm.Dispose();
			}

			return synopses;
		}


		private IList<Genre> GetFilmGenres(int titleID)
		{
			List<Genre> genres = new List<Genre>();

			using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["TitlesEntities"].ConnectionString))
			{
				conn.Open();

				string sql = string.Format("SELECT Name FROM Genre g JOIN TitleGenre tg ON g.Id = tg.GenreId WHERE TitleID = '{0}' ORDER BY Name", titleID.ToString());
				SqlCommand comm = new SqlCommand(sql, conn);
				SqlDataReader dr = comm.ExecuteReader();
				if (dr.HasRows)
				{
					Genre genre;
					while (dr.Read())
					{
						genre = new Genre();
						genre.Name = dr["Name"].ToString();

						genres.Add(genre);
					}
				}

				dr.Close();
				dr.Dispose();

				comm.Dispose();
			}

			return genres;
		}


		private IList<Award> GetFilmAwards(int titleID)
		{
			List<Award> awards = new List<Award>();

			using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["TitlesEntities"].ConnectionString))
			{
				conn.Open();

				string sql = string.Format("SELECT Award, AwardCompany, AwardYear, AwardWon FROM Award WHERE TitleID = '{0}' ORDER BY AwardCompany, AwardYear, AwardWon DESC, Award", titleID.ToString());
				SqlCommand comm = new SqlCommand(sql, conn);
				SqlDataReader dr = comm.ExecuteReader();
				if (dr.HasRows)
				{
					Award award;
					while (dr.Read())
					{
						award = new Award();
						award.Name = dr["Award"].ToString();
						award.Company = dr["AwardCompany"].ToString();
						award.Year = dr["AwardYear"].ToString();
						award.Won = Convert.ToBoolean(dr["AwardWon"].ToString());

						awards.Add(award);
					}
				}

				dr.Close();
				dr.Dispose();

				comm.Dispose();
			}

			return awards;
		}


		private IList<Participant> GetFilmParticipants(int titleID)
		{
			List<Participant> participants = new List<Participant>();

			using (SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["TitlesEntities"].ConnectionString))
			{
				conn.Open();

				string sql = string.Format("SELECT Name, ParticipantType, RoleType, IsKey, IsOnScreen FROM Participant p JOIN TitleParticipant tp ON p.Id = tp.ParticipantId WHERE TitleID = '{0}' ORDER BY IsKey DESC, IsOnScreen DESC, CASE RoleType WHEN 'Producer' THEN 'a' WHEN 'Director' THEN 'b' WHEN 'Actor' THEN 'c' ELSE 'z' END + RoleType, RoleType, CASE ParticipantType WHEN 'Group' THEN Name ELSE CASE CHARINDEX(' ', Name) WHEN 0 THEN Name ELSE RIGHT(Name, CHARINDEX(' ', REVERSE(Name)) - 1) END END", titleID.ToString());
				SqlCommand comm = new SqlCommand(sql, conn);
				SqlDataReader dr = comm.ExecuteReader();
				if (dr.HasRows)
				{
					Participant participant;
					while (dr.Read())
					{
						participant = new Participant();
						participant.Name = dr["Name"].ToString();
						participant.Type = dr["ParticipantType"].ToString();
						participant.Role = dr["RoleType"].ToString();
						participant.IsKey = Convert.ToBoolean(dr["IsKey"].ToString());
						participant.IsOnScreen = Convert.ToBoolean(dr["IsOnScreen"].ToString());

						participants.Add(participant);
					}
				}

				dr.Close();
				dr.Dispose();

				comm.Dispose();
			}

			return participants;
		}
	}
}