﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using FilmSearch.DataAccess;
using FilmSearch.Models;

namespace FilmSearch.Controllers
{
	public class ValuesController : ApiController
	{
        public IEnumerable<Film> Get()
        {
            return new FilmData().GetFilms();
        }

		public IEnumerable<Film> Get(int id)
		{
			List<int> idList = new List<int>();
			idList.Add(id);

			return new FilmData().GetFilmsDetail(idList);
		}

        public IEnumerable<Film> Get(int id, string titleSearch)
        {
            return new FilmData().GetFilms(titleSearch);
        }
    }
}