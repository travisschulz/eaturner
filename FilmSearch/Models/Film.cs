﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilmSearch.Models
{
    public class Film
    {
        public int TitleID { get; set; }
        public Title Title { get; set; }
        public List<Title> AltTitles { get; set; }
        public string ReleaseYear { get; set; }
        public List<Synopsis> Synopses { get; set; }
        public List<Genre> Genres { get; set; }
        public List<Award> Awards { get; set; }
        public List<Participant> Participants { get; set; }
    }
}