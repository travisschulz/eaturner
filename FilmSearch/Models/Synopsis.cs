﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilmSearch.Models
{
    public class Synopsis
    {
        public string Type { get; set; }
        public string Language { get; set; }
        public string Description { get; set; }
    }
}