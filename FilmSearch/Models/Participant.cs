﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilmSearch.Models
{
    public class Participant
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Role { get; set; }
        public bool IsKey { get; set; }
        public bool IsOnScreen { get; set; }
    }
}