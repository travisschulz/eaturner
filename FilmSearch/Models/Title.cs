﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilmSearch.Models
{
    public class Title
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Language { get; set; }
        public string Sortable { get; set; }
    }
}