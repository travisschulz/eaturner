﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilmSearch.Models
{
    public class Award
    {
        public string Name { get; set; }
        public string Company { get; set; }
        public string Year { get; set; }
        public bool Won { get; set; }
    }
}